import cmd


class GotCmd(cmd.Cmd):

    def __init__(self, controller):
        self.controller = controller
        cmd.Cmd.__init__(self)

    def start(self):
        self.cmdloop()

    def do_character_search(self, args):
        try:
            self.controller.scrape_data(args)
        except IndexError:
            print('No data could be found for that character')

    def do_show_data(self):
        try:
            self.controller.show_data()
        except IndexError:
            print('No data could be found for that character')