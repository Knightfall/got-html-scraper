from bs4 import BeautifulSoup
from urllib.request import urlopen
import csv
import sys
import os


"""THIS FILE IS NOT USED AT ALL IN THE ACTUAL SCRAPER, THIS IS FOR TESTING PURPOSES"""


# gets the first and last year to look for the  top 100 songs from

print("What character are you looking for? (First and last name)")
character_name = sys.stdin.readline()

# creates data list to store the data into
data = []


def scrape_data():
    char_data = []
    url = 'https://en.wikipedia.org/wiki/' + character_name
    print(url)
    r = urlopen(url)

    soup = BeautifulSoup(r, 'html.parser')
    table = soup.find('table', attrs={'class': 'infobox'})
    rows = table.find_all('tr')

    for row in rows[0:]:
        cols = row.find_all(['td', 'th'])
        char_data.append([col.text for col in cols])

    return char_data

try:
    data.append(scrape_data())
    print('Scraping data....')
except AttributeError as e:
    print('data not found')

for info in data:
    for details in info:
        print(details)

# writes the information to a CSV file
if 'characters.csv' in globals():
    os.remove("characters.csv")
resultFile = open("characters.csv", 'w')
writer = csv.writer(resultFile, dialect='excel')
for info in data:
    for details in info:
        writer.writerow(details)

