import got_cmd
import got_scraper
import sys
import getopt


def main(args):

    scraper = got_scraper.Scraper()
    if args is []:
        cmd = got_cmd.GotCmd(scraper)
        cmd.start()
    else:
        try:
            opts, fhqwhgads = getopt.getopt(args[1:],
                                       "hafcork",
                                       ["help",
                                        "actor",
                                        "fAppearence",
                                        "aliases",
                                        "house",
                                        "relatives",
                                        "kingdom"])

        except getopt.GetoptError:
            # display help message etc
            sys.exit(2)

        try:
            scraper.scrape_data(args[0])
        except IndexError:
            print('No data could be found for that character')

        for opt, arg in opts:
            if opt in ("-h", "--help"):
                print("""Wanted arguments (never got implemented)Arguments:
                        -a 	Actor that portrays the character
                        -f 	First appearance in the TV series
                        -l 	Last appearance in the TV series (if applicable)
                        -c 	Aliases of the character
                        -h 	House name of the character
                        -r 	Relatives of the character
                        -k 	Kingdom from which the character originates""")
                sys.exit()

            if opt in ("-a", "--actor"):
                print(scraper.get_information("Portrayed by"))

            if opt in ("-f", "--fAppearence"):
                print(scraper.get_information("First appearance"))

            if opt in ("-c", "--aliases"):
                print(scraper.get_information("Aliases"))

            if opt in ("-o", "--house"):
                print(scraper.get_information("Family"))

            if opt in ("-r", "--relatives"):
                print(scraper.get_information("Relatives"))

            if opt in ("-k", "--kingdom"):
                print(scraper.get_information("Kingdom"))


if __name__ == '__main__':
    main(sys.argv[1:])
