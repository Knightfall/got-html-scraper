from abc import ABCMeta, abstractmethod


class Controller(metaclass=ABCMeta):

    @abstractmethod
    def get_character_name(self):
        pass

    @abstractmethod
    def scrape_data(self, args):
        pass

    @abstractmethod
    def show_data(self, data):
        pass

    @abstractmethod
    def pickle_data(self, data):
        pass
