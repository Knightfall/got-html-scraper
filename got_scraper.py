from bs4 import BeautifulSoup
from urllib.request import urlopen
import sys
from sys import stdin
import controller
import csv
import shelve


class Scraper(controller.Controller):

    character_data = []

    # searches the internet for the wiki link to the GoT character page, saves it to a doc and displays
    def scrape_data(self, args):
        url = 'https://en.wikipedia.org/wiki/' + args
        print('Searching....')
        r = urlopen(url)

        # looks for the table element on the page to pull data from
        soup = BeautifulSoup(r, 'html.parser')
        table = soup.find('table', attrs={'class': 'infobox'})
        rows = table.find_all('tr')

        # uses list comprehension to save the table row data to a list
        char_data = []
        for row in rows[0:]:
            cols = row.find_all(['td', 'th'])
            char_data.append([col.text for col in cols])

        self.character_data = char_data
        # returns the character data list for the specified char
        return char_data

    def get_character_name(self):
        # gets the first and last name of the character to search
        print("What character are you looking for? (First and last name)")

        character_name = stdin.readline()
        return character_name

    # shows the data of the scraped information
    def show_data(self, data):
        for info in data:
            for details in info:
                print(details)

    def get_information(self, information_tag):
        for item in self.character_data:
            if item[0] == information_tag:
                return item[1]


    @staticmethod
    def get_csv_file():
        # on command open the csv file to view all past pulled information
        with open("characters.csv", 'rb') as f:
            reader = csv.reader(f)
            try:
                for row in reader:
                    print(row)
            except csv.Error as e:
                sys.exit('file %s, line %d: %s' % ("characters.csv", reader.line_num, e))

    def pickle_data(self, data):
        pickle_file = shelve.open("pickled_data.dat")
        pickle_file["char_data"] = data
