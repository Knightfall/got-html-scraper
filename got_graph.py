import plotly.plotly as py
import plotly.graph_objs as gr

labels = ['Oxygen', 'Hydrogen', 'Carbon_Dioxide', 'Nitrogen']
values = [4500, 2500, 1053, 500]

trace = gr.Pie(labels=labels, values=values)

py.iplot([trace])
