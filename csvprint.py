import csv
import sys

with open("characters.csv", 'r') as f:
    reader = csv.reader(f)
    try:
        for row in reader:
            print(row)
    except csv.Error as e:
        sys.exit('file %s, line %d: %s' % ("characters.csv", reader.line_num, e))
